Autistici.org website
=====================

This is the private repository containing the source code for the autistici.org
website.

Contents are written in Markdown, and rendered to static HTML at
deployment time.

# Making changes

The website contents are located in the `src` subdirectory. Pages are
encoded in Markdown, with a small header providing page metadata
(mostly the page title).

The header is YAML-encoded, and it must be separated from the page
content with a line containing only four dashes (`----`).  For
example:

    title: About
    ----
    Contents of the about page.

When editing the website sources, it is a good idea to test the
results locally, before submitting the changes. In order to do so,
there are some setup steps you will need to perform on your system.

## Requirements

To build the website, a few tools will need to be installed:
[gostatic](https://github.com/piranha/gostatic) to generate the HTML
pages, and [sitesearch](https://git.autistici.org/ai/sitesearch) to
generate the search index. The scripts will automatically download and
install these tools if necessary, but you will need a working
[Go](https://golang.org/) development environment.  Furthermore,
testing the website requires running a local Apache server.

On a Debian system, you can install all the requirements with:

    $ sudo apt-get install rsync golang-go

## Testing changes for correctness

There are a few static checks that you can run after modifying the
website. They will check for broken internal links, bad syntax, and
other things. You can invoke the lint script that runs these checks
with:

    $ ./scripts/lint.sh

If this scripts does not output anything, the tests have passed
successfully.

It is a good idea to set up the linter script as a git *pre-commit*
hook, to make it impossible to commit a change that does not pass
these tests successfully:

    $ cat >.git/hooks/pre-commit <<EOF
    #!/bin/sh
    exec ./scripts/lint.sh
    EOF
    $ chmod +x .git/hooks/pre-commit

## How to build the website

Simply run, from the top-level directory:

    $ ./scripts/update.sh

The resulting HTML pages will be found in the `public` directory.

## How to run a test webserver

### Docker

The preferred way, which avoids installing an entire Debian
distribution, would be to use Docker. Build the image with:

    $ docker build -t ai-website .

and run it with:

    $ docker run -p 8080 --network host ai-website

it should then be visible at http://localhost:8080/.

### debootstrap

To check the results of your edits, it is useful to start a local
webserver to inspect the generated HTML pages. In general, you
might want to avoid installing the Apache packages on your local
system (especially on Debian where they automatically start
daemons), so we have prepared a way to install Apache in a
chroot. Run the following commands just once to set it up:

    $ sudo apt-get install debootstrap
    $ sudo ./scripts/install-apache.sh

Then, to start Apache and the search daemon, run (it may ask
you for your password due to the use of *sudo*):

    $ sudo ./scripts/run-test-server.sh

This will start a full server stack (web server and search server)
that you can access by browsing to

    http://localhost:3300

To stop the Apache process when you're done, run:

    $ sudo ./scripts/run-test-server.sh stop


## Updating the FAQ

FAQ pages are currently stored in an old format (compatible with
the *makefaq* tool). Just update them regularly and `update.sh`
will handle them correctly.

## Embedding external data sources

It is possible to use dynamically-generated data in the templates by
placing a script in the `data.d/` directory. The script must be
executable, and its name should not contain dots. Its output must
be valid JSON.

Data from these scripts will be collected in a single dictionary
in the file `data.json`: the data returned by each script will be
present with a key named as the script.

Files ending in `.md_in` will be preprocessed by the template engine
(using the Go html/template syntax) before being rendered into HTML.

As an example of this technique, you can check out:

* `data.d/dns`
* `src/docs/web/domains.en.md_in`



