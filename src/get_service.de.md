title: Einen Dienst beantragen
----

# Einen Dienst beantragen

Einige der von A/I bereitgestellten Dienste, wie [IRC](/services/chat "IRC - Chat") und die Anonymisierungsdienste, sind zur
freien Nutzung durch alle gedacht, die sich im Netz bewegen, während andere "persönliche" Dienste der Erstellung eines *Accounts*
bedürfen.

Für die Registrierung benötigen wir keinerlei persönliche Angaben, ausser einer Email-Adresse an die wir die Aktivierungsdaten senden
können (alle beantragten Dienste müssen zunächst freigeschaltet werden). Dieser Umstand könnte euch Anlass geben mal darüber nachzudenken,
wie schwer es doch ist, die eigenen Spuren vollständig zu verwischen... (wir werden die Email-Adresse, die ihr uns bei der Registrierung
nennt, weder speichern, noch anderweitig aufbewahren. Aber der Betreiber des Postfachs, das Ihr dazu benutzt, könnte dies tun...)

Alles was wir für die Bereitstellung eurer Dienste von euch verlangen, ist dass ihr unsere Grundsätze hinsichtlich Antifaschismus,
Antisexismus, Antirassismus, Antimilitarismus und nicht-Kommerzialisierung teilt. Lest daher unsere [Policy](/who/policy) und unser
[Manifest](/who/manifesto), bevor ihr einen Account beantragt.

[Privacy Policy](/who/privacy-policy)

Wir möchten darauf hinweisen, dass **unsere Dienste zwar frei verfügbar sind, dies aber nicht bedeutet, dass sie auch kostenlos wären**. Es
bedeutet vielmehr, dass wir ein Selbstverständnis haben, das auf Selbstverwaltung basiert und wir uns ausserhalb jeglicher kommerzieller
Interessen und Machtspielchen bewegen möchten. Es ist daher besonders wichtig, dass ihr ausser eure Dienste zu nutzen, euch auch mit um das
Fortbestehen dieses Webangebots kümmert. Da es um unsere Server am Laufen zu halten mehr bedarf, als der blossen täglichen Wartung (die auf
freiwilliger Basis und ohne jegliche Vergütung stattfindet), wären wir euch dankbar, wenn auch ihr zumindest einen kleinen Beitrag leisten
könntet, um so die laufenden Kosten für Housing, Bandbreite, Wartung der Hardware usw. mitzutragen.
[Hier erfahrt ihr, wie ihr Autistici/Inventati eine Spende zukommen lassen könnt](/donate "donate").

Um einen Account oder Dienst auf den Servern von A/I zu beantragen, folgt diesen Schritten:

## 1 - Lest aufmerksam unsere [Policy](/who/policy)

Wir setzen voraus, dass diejenigen, die einen Dienst bei uns beantragen, die Grundsätze unserer Policy uneingeschränkt teilen.

## 2 - Folgt diesem Link [https://services.autistici.org/](https://services.autistici.org/)

und füllt das Formular für die Freischaltung eines neuen Dienstes aus: fügt alle Angaben korrekt ein und vergesst nicht, eine Email-Adresse anzugeben, damit wir euch kontaktieren können. Es würde uns zudem freuen zu erfahren, was ihr mit dem neu beantragten Dienst vor habt. Wir schätzen unsere User, und haben Gefallen an dem Gedanken, dass auch diese die Bereitschaft besitzen, ihre Pläne und Projekte mit uns zu teilen.

## 3 - Wartet die Bestätigungsmail ab

bzw. die Ablehnung, falls
wir zu dem Schluss kommen sollten, dass euer Antrag sich nicht mit unserer
Policy vereinbaren lässt. In dieser findet ihr alle notwendigen Angaben und
Anleitungen, um Zugang zu euerem [Verwaltungsbereich](/docs/userpanel) und zu euerem
Email-Konto zu erlangen. Das Kollektiv A/I tut sein Bestes, um alle Anfragen zu
bearbeiten, also habt Vertrauen, früher oder später werden wir euch
antworten...

