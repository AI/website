title: Collettivi tech amici 
----

Collettivi tech amici
-----------------------

### Server anonimi e autogestiti che offrono mail e altri servizi con policy compatibili

- [ecn.org](http://www.ecn.org) (è un luogo di visibilità, di relazione e di possibile ricomposizione per quei soggetti che i profondi mutamenti della nostra società in questi anni hanno frammentato e disperso, i soggetti non allineati al pensiero unico o rassegnati alla marginalità, i soggetti ancora desiderosi di costruire un movimento reale, che sappia cambiare lo stato di cose presenti - italia)
- [indivia.net](http://indivia.net) (Mailing list, streaming audio, spazio web, Irc chat, e-mail, forum, Dns dinamici su un server vegetale a foglia lunga verde chiaro - italia)
- [oziosi.org](http://www.oziosi.org) (Oziosi.org riconosce la pigrizia, la lentezza e l'ozio come uno stato di natura a cui l'uomo e la donna liberi desiderano con calma - italia)
- [free.de](http://www.free.de) (uno storico progetto tedesco che offre caselle di posta, mailing list e siti web agli attivisti - germania)
- [resist.ca](http://resist.ca) (gruppo di attivisti di Vancouver che offre informazioni e formazione tecnologica agli attivisti e fornisce servizi per la comunicazione come caselle di posta e mailing list - canada)
- [riseup.net](http://riseup.net) (riseup.net fornisce caselle di posta, mailing list e siti web a persone impegnate per un cambiamento sociale libertario - u.s.a.)
- [squat.net](http://squat.net) (squat!net è una rivista online dedicata alle occupazioni che offre servizi internet agli spazi occupati- olanda)
- [so36.net](http://so36.net) (server indipendente - germania)
- [nadir.org](http://nadir.org) (Un progetto per una revisione dei principi della sinistra attraverso la creazione di un luogo di comunicazione e informazione - germania)
- [aktivix.org](http://aktivix.org) (server indipendente - fornisce email, mailing list e vpn)
- [boum.org](http://boum.org) (server autogestito, anonimo, tengono la macchina fuori dalla francia - francia)
- [shelter.is](https://shelter.is) (collettivo tech che fornisce servizi nel rispetto della privacy)
- [espiv.net](https://espiv.net) (collettivo cybrigade che fornisce email, mailing list, blog - grecia)

  
### Un elenco, incompleto, di progetti con cui abbiamo qualcosa in comune...
  
- [freaknet.org](http://www.freaknet.org) (Laboratorio di sperimentazione di tecnologie della comunicazione - Catania - italia)
- [interactivist.net](http://interactivist.net/) (Progetto di comunicazione militante e indipendente e di condivisione delle conoscenze tecnologiche - u.s.a.)
- [mutualaid.org](http://mutualaid.org) (Server per comunità radicali - u.s.a.)
- [nodo50.org](http://nodo50.org) (progetto autonomo di informazione telematica - spagna)
- [tao.ca](http://tao.ca) (progetto anarchico di un piccolo collettivo di Toronto teso a diffondere le conoscenze tecnologiche - canada)

