title: No entre en pánico!
----

No entre en pánico!
===================

¿Cómo encontrar ayuda en A/I?
-----------------------------

En esta página intentamos explicarte qué puedes hacer cuando piensas que necesitas nuestra ayuda. Hemos creado algunas herramientas para
intentar reducir el tráfico de correos electrónicos. Así que, antes de escribirnos, **te invitamos calurosamente a que encuentres aquí la
solución a tu problema**.

- [Cavallette](https://cavallette.noblogs.org/) - News?
- [Preguntas frecuentes](/docs/faq) (con sus correspondientes respuestas :) )
- [Manuales](/docs/) - Los manuales de nuestros servicios.
- [Helpdesk](http://helpdesk.autistici.org/) - Aquí puedes enviarnos una pregunta específica para tu problema, y te contestaremos lo antes que sea posible (¡deja una cuenta de correo a la que podamos contestar!)

Generalmente, también puedes encontrar a algun\*s de nosotr\*s en el canal de IRC **\#ai**
de nuestro servidor [autistici.org](/docs/irc/). Con tiempo, alguien te dará consejos e información, o podrá
ayudarte a resolver pequeños problemas técnicos.

<a name="gpgkey"></a>

Si estás realmente desesperad\* y no sabes que hacer,
[escríbenos](mailto:info@autistici.org), posiblemente usando nuestra [clave GPG](gpg_key) (disponible en muchos servidores de claves).

