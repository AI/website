title: Wer wir sind
----

Wer wir sind
============

A/I ist ein Projekt, das vor mehr als zehn Jahren entstand, um Personen und Projekte aus basisdemokratischen und sozialen Bewegungen durch
das Anbieten von Web-Hosting, E-Mail-Konten, Mailinglistem, Chat-Räumen, Instant-Messaging, Remailern, Blogs, Newslettern und vielen
weiteren Diensten im Internet zu unterstützen. Unsere Überzeugung ist einfach: Die Welt soll sich nicht um Geld drehen, sondern in sicheren
Bahnen gehalten werden durch ein stabiles Geflecht aus Beziehungen, die auf Prinzipien wie Solidarität, gegenseitiger Unterstützung,
Gleichheit und sozialer Gerechtigkeit beruhen. Klingt einfach, oder? Wir möchten sowohl Personen als auch Projekte unterstützen, deren
politische und soziale Gesinnung der unseren gleicht und die ebenfalls folgende Werte mit uns teilen: **Antifaschismus, Antisexismus,
Antirassismus und Antimilitarismus**. Dem hinzufügen müssen wir noch ein Gefühl, eine Haltung: **das tiefe Unbehagen der Logik des Geldes
gegenüber**.

Unsere Ressourcen werden durch die Spenden der Gemeinschaft garantiert, die sie nutzt - die Arbeit der gemeinsamen Verwaltung ist streng
**freiwillig**. Wir unterstützen keine kommerziellen Strukturen, die sich mit Sicherheit an anderer Stelle wohler fühlen werden. Ebensowenig
befürworten wir institutionelle oder parteiliche Strukturen, die allein aufgrund ihrer Natur auf Ressourcen zurückgreifen können, die nicht
- wie unsere - freiwillig und selbst-finanziert sind. Unsere Dienste wurden entworfen, um **die Meinungsfreiheit und Privatsphäre ihrer
Nutzer zu schützen**, folglich bewahren wir keine sensiblen Daten über sie auf und betreiben keine Form des kommerziellen Data-Mining.

Um mehr über uns und unsere Ziele zu erfahren:

* Unsere [Richtlinien](/who/policy)
* [privacy policy](/who/privacy-policy)
* Eine [kurze Geschichte](/who/telltale) über die Gründe, warum wir sind, wer wir sind, und warum wir tun, was wir tun.
* Unser [Manifest](/who/manifesto), das wir 2002 geschrieben haben, um zu erklären wer wir sind und was wir wollen
* Ein paar Zeilen über [unsere Geschichte als Gemeinschaft](/who/collective) (weniger erzählerisch mehr historisch)
* [Das Buch zum 10-jährigen Jubiläum unserer Gemeinschaft](/who/book)
* [Bekanntmachungen, Flyer, Website-Banner und Grafiken](/who/propaganda)
* Der [R\*-Plan](/who/rplan)
* [Infrastrukturkosten](/who/costs)

Unsere Server werden von der "Associazione AI-ODV" betrieben. Bei oder Problemen kontaktieren sie bitte: info (at)
investici.org.
