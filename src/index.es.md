title: Bienvenid@s a Autistici/Inventati
----

Bienvenid@s a Autistici/Inventati.
===================================

A/I nació hace más de 10 años cuando individuos y colectivos ocupad\*s en cuestiones de tecnología, privacidad,
derechos digitales y activismo político se reunieron en Italia. Nuestra idea básica es proveer una amplia variedad de herramientas de
comunicación libres, impulsando a las personas a elegir aquellas opciones libres en lugar de las comerciales. Queremos informar y formar
sobre la necesidad de proteger nuestra propia privacidad con el fin de evitar el saqueo que gobiernos y corporaciones llevan a cabo de forma
indiscriminada sobre nuestros datos y personalidades.

A/I [ofrece](/get_service "Pregunta por tu cuenta!") una amplia gama de [servicios](/services/) gratuitos, libres y
respetuosos de la privacidad, entre los que se incluyen:

[Blogs](/services/blog "Blog sobre Noblogs.org") / [Alojamiento web](/services/website "Alojamento web sobre A/I")
[Servicios de anonimización](/services/anon "Servicio de anonimización sobre A/I") / [VPNs personales](https://vpn.autistici.org "Personal A/I VPNs")
[Correo electrónico](/services/mail "Correos electrónico de A/I") / [Listas de correo, Newsletters y Foros](/services/lists "Listas sobre A/I")
[Mensajería Instantánea y Chats](/services/chat "Instant Messaging and Chat on A/I") y [Mas](/services).

Todas las solicitudes de servicios se aprueban siempre y cuando respeten nuestra [política](/who/policy) y compartan
nuestro [manifiesto](/who/manifesto).

**[Solicita un servicio](/get_service)**

**[Blog](https://noblogs.org/register/)**
