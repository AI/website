title: Qui sommes-nous?
----

Qui sommes-nous?
================

A/I est né il y a plus de 10 ans quand des individus et des collectifs travaillant autour de la technologie, de l'anonymat, des cyber-droits
et de l'activisme politique se sont rencontrés en Italie. Notre but premier est de fournir des outils de communication libres à une large
base, et ainsi de donner la possibilité de s'affranchir des modes de communication commerciaux. Nous voudrions amener à la conscience des
gens le besoin de protéger leur vie privée et d'échapper au pillage des données perpétré par les gouvernements et les entreprises.

Nous essayons de réaliser cet objectif en proposant des services internet (sites web, e-mail, mailing-listes, chat, messagerie instantanée,
remailer anonyme, blogs, newsletters, et plus) à la fois aux individus et aux projets collectifs qui rejoignent nos objectifs et partagent
nos idéaux, et nous utilisons nos savoir-faire et notre connaissance pour défendre la vie privée des utilisateurs.

Désertant l'attitude commerciale des services et des espaces web monnayés, nous accueillons avec joie ceux qui ne se satisfont ni de la
censure culturelle et médiatique ni de l'imaginaire mondialisé qu'on nous prépare, qu'on nous emballe et qu'on nous vend jour après jour.

Pour en savoir plus à propos de nous et de nos objectifs:

* Nos [conditions d'utilisateur](/who/policy)
* [privacy policy](/who/privacy-policy)
* Le [manifeste](/who/manifesto) que nous avons écrit en 2002 pour expliquer qui nous sommes et ce que nous voulons
* Quelques mots sur [l'histoire du collectif](/who/collective)
* [Propagande, flyers, bannières et éléments graphiques](/who/propaganda)
* Le [Plan R\*](/who/rplan)
* [Coûts](/who/costs) d'infrastructure

Nos serveurs sont gérés légalement par l'[association AI-ODV](mailto:associazione@ai-odv). 
For technical problems, write to [helpdesk](mailto:help@autistici.org)
Pour autre problème, merci de nous écrire à l'adresse : info at investici.org.


