title: Como contribuir con el proyecto 
----

Como doar para o projeto?
=========================

Nosso projeto vive **EXCLUSIVAMENTE** graças às contribuições das pessoas,
então ficaremos muito felizes e agradecidos por qualquer valor com que
você possa nos ajudar.

Nesta página você poderá encontrar variadas maneiras de doar, não seja
tímid\*! Se você quer saber quanto custa para manter tudo rodando e funcionando,
por favor *dê uma [olhada](/who/costs)!*

Desculpa, mas não aceitamos Bitcoin. *[Este
post](http://cavallette.noblogs.org/2013/07/8333#eng)* explica o motivo.

- [Transferência Bancária para o a Banca Etica](#bonifico)
- [Doação Online](#carta) ((Você precisará de um cartão de crédito)
- [Vida real](#life) (se você conhece e confia em alguém deste projeto)

Detalhes
--------

### Transferência Direta a Banca Etica

<a name="bonifico"></a>

    Associazione AI ODV 
    EU IBAN: IT83P0501802800000016719213 
    BIC/SWIFT CODE: CCRTIT2T84A
    Banca Popolare Etica - Agencia de Milán
    V. Santa Tecla, 5
    Reason/Razão: doação para Investici (ano 201x)
    
### Doação Online

<a name="carta"></a>

Você pode fazer sua doação usando um cartão de crédito. Para a transação
usamos o PayPal (você não precisa criar uma conta). Nós não gostamos
muito do PayPal, mas ainda não encontramos nenhuma alternativa melhor.
Se você tem alguma sugestão, por favor, sinta-se livre em recomendar
para nós.

Por: E-mail/Lista de E-mail/Hospedagem/NoBlogs/REVOLUÇÕES! <<input>> (em
euros)

<form action="https://www.paypal.com/cgi-bin/webscr" method="post">
<table>
<tbody>
<tr class="odd">
<td align="left"><input name="on0" value="Per" type="hidden">Por</td>
<td align="left"><select name="os0"> <option selected="selected" value="E-mail">E-mail</option> <option value="Mailing-List">Lista de E-mail</option> <option value="Hosting">Hospedagem</option> <option value="NoBlogs">NoBlogs</option> <option value="Revolutions!">REVOLUÇÕES!</option> </select> <input size="6" name="amount" value="25.00" type="text"> <small>(em Euros)</small></td>
</tr>
</tbody>
</table>

  
<input name="cmd" value="_xclick" type="hidden"> <input name="business" value="donate@inventati.org" type="hidden">
<input name="item_name" value="Donazione per A/I Servers" type="hidden"> <input name="item_number" value="10011" type="hidden">
<!-- input type="hidden" name="amount" value="25.00" / --> <input name="no_shipping" value="1" type="hidden">
<input name="no_note" value="1" type="hidden"> <input name="currency_code" value="EUR" type="hidden">
<input name="tax" value="0" type="hidden"> <input name="lc" value="IT" type="hidden">
<input type="image" src="https://www.paypal.com/en_US/i/btn/x-click-but11.gif" border="0" name="submit" alt="[Donate]">
</form>

### Doação Recorrente 

Essa é uma doação recorrente.
Se você quer ter certeza em assinar para nosso projeto anualmente, você
poderá usar o formulário seguinte.
A cada ano, 25 euros (ou o valor que você quiser) serão creditados em
nossa conta off-shore  (nós estamos brincando sobre isso, claro).

<form action="https://www.paypal.com/cgi-bin/webscr" method="post">
<table>
<tbody>
<tr class="odd">
<td align="left"><input name="on0" value="Perche'" type="hidden">Por</td>
<td align="left"><select name="os0"> <option selected="selected" value="E-mail">E-mail</option> <option value="Mailing-List">Lista de E-mail</option> <option value="Web-Hosting">Hospedagem</option> <option value="NoBlogs">NoBlogs</option> <option value="Legal Support">Legal Support</option> <option value="Revolution!">REVOLUÇÕES!</option> </select> <input size="6" name="a3" value="50.00" type="text"> <small>(en Euros)</small></td>
</tr>
</tbody>
</table>

<input type="image" src="https://www.paypal.com/en_US/i/btn/x-click-but12.gif" border="0" name="submit" alt="[Subscribe]">
<input name="cmd" value="_xclick-subscriptions" type="hidden"> <input name="business" value="donate@inventati.org" type="hidden">
<input name="item_name" value="Sottoscrizione Ricorsiva ad
A/I" type="hidden"> <input name="item_number" value="10012" type="hidden"> <input name="no_shipping" value="1" type="hidden">
<input name="no_note" value="1" type="hidden"> <input name="currency_code" value="EUR" type="hidden">
<input name="lc" value="IT" type="hidden"> <!-- input type="hidden" name="a3" value="50.00" / --> <input name="p3" value="1" type="hidden">
<input name="t3" value="Y" type="hidden"> <input name="src" value="1" type="hidden"> <input name="sra" value="1" type="hidden">
</form>

### Vida Real

<a name="life"></a>

Se você reside próximo à alguém do projeto que você conhece e confia,
você pode dar a ele/ela sua doação.
É importante que você tente coletar diferentes doações (de coletivos,
amigos ou equipes) antes de doar o dinheiro, assim a pobre pessoa que
receber o dinheiro poderá fazer como uma simples transação (ao invés de
múltiplas).
