title: Connecting to IRC with Pidgin 
----

Connecting to IRC with Pidgin
=============================

You may already be using Pidgin and want to stay with something familiar and discreet. Pidgin can connect to IRC and can encrypt your
connection with SSL (if the network you're using supports encryption – autistici does). This is good if you don't want to install something
unfamiliar or specifically for IRC but it does mean that you won't have all the features of IRC.

1.  Open Pidgin and open the account manager from the top menu: Accounts -- Manage Accounts
2.  Click the green + sign to add a new account
3.  Fill in the first tab on the form like this (obviously pick a nickname of your own): ![pidgin account manager](/static/img/man_irc/en/pidgin1.png)
4.  Now click the 'advanced' tab and fill it in like this:
     ![pidign account manager advanced tab](/static/img/man_irc/en/pidgin2.png)
5.  Now click the 'add' button at the bottom of the form. Pidgin will try to connect and then it will throw up this screen:
     ![certificate screen in pidgin](/static/img/man_irc/en/pidgin3.png)
6.  Click the 'accept' button and Pidgin should connect you to the network
7.  Now you'll see a window like the one below. Just one more thing to do, you need to join your channel of choice – in the chat input
    window (where you normally type what you want to say) type this exactly (including the slash and hash symbols — look at the illustration
    below): /join \#channelname
     ![join the channel](/static/img/man_irc/en/pidgin4.png)
8.  Now click enter/send and Pidgin will join you to the channel. You'll see a list of the other people in the channel on your right and the
    usual chat and input panes on your left. Chat as you normally would in Pidgin chat.
9.  You will find some of the commands you may need in [this howto](/docs/irc/)
