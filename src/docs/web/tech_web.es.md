title: Características técnicas del hosting web
----

Características técnicas del hosting web
===============================================

En esta página encontrará las características técnicas principales del servicio de hosting web de A/I.

Por favor, consulte también nuestras [FAQ](/docs/faq/) y nuestras páginas sobre el [alojamiento de sitios web](/services/website)
para más documentación.

### URL del sitio

La URL principal desde la que se podrá acceder a su sitio es:

> `http://www.autistici.org/susitio/`

(o `www.inventati.org/susitio/`). A/I usa un proxy para redirigir automáticamente a los visitantes al servidor en donde esté alojado su
sitio.

### Uso del espacio de alojamiento

El espacio de alojamiento por sitios web ofrecido por A/I no tiene límites estrictos. Esto no quiere decir que puede abandonar toda
responsabilidad al usar su espacio: para mantenerle actualizado, siempre se le informará a través del panel de usuarios sobre el espacio
total utilizado en nuestros servidores. Cuanto menos espacio utilice, ¡menor será la sobrecarga en nuestras estructuras!


### Scripts CGI

Debido a como están organizados los servicios, por el momento A/I **no ofrece** la posibilidad de ejecutar scripts CGI.
