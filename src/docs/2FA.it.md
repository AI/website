title: Autenticazione a due fattori
----

Proteggi il tuo account con l'autenticazione a due fattori
==========================================================

L'autenticazione a due fattori (o 2FA) è un meccanismo di autenticazione
che aumenta il livello di sicurezza del tuo account evitando che
qualcuno possa soffiarti la password online per accedere al tuo pannello
utente e alla tua webmail.
 Il concetto è semplice: una volta che avrai attivato questo sistema,
per entrare nel tuo pannello utente e nella tua webmail avrai bisogno di
*una cosa che sai* (la tua password) e di *una cosa che hai* (il tuo
smartphone, in genere).
 In questo modo diventerà più difficile per qualche malintenzionato
accedere al tuo account, perché anche se qualcuno riuscisse a
intrufolarsi in qualche modo nel tuo computer e a soffiarti la password,
avrebbe bisogno anche di rubarti il telefono per entrare nel pannello e
nella webmail.

Come attivare l'autenticazione a due fattori
--------------------------------------------

Per attivare l'autenticazione a due fattori bastano pochi passi:

1.  Accedi al tuo [pannello
    utente](https://accounts.autistici.org).
2.  Clicca sul tuo nome utente in alto a destra sulla barra nera.
3.  Nel menu a tendina che si è appena aperto, clicca su "Abilita
    autenticazione a doppio fattore (OTP)".
     ![](/static/img/man_2fa/2FA1.png "attiva 2FA")
4.  Segui le istruzioni che si apriranno subito dopo (in sostanza devi
    installare un'app sul tuo telefono, impostare la domanda di recupero
    della password se non l'hai ancora fatto e alla fine cliccare sul
    tasto "Abilita").
     ![](/static/img/man_2fa/2FA2.png "istruzioni per
          l'attivazione 2FA")
5.  Da questo momento in poi, quando vorrai accedere al tuo pannello
    utente, dopo avere inserito il tuo nome utente e la tua password si
    aprirà un'altra schermata che ti chiederà un codice "OTP": per
    accedere al pannello, apri l'app sul tuo telefono e inserisci nel
    form sul sito il codice che si visualizza nell'app.
     ![](/static/img/man_2fa/2FA.png "richiesta OTP")

Usare client di posta e jabber con la 2FA
-----------------------------------------

La 2FA è per il web: per il resto dovrai generare una *password
specifica*. Per i client di posta come Thunderbird e per Jabber/XMPP
(Pidgin, Jitsi) avrai bisogno di una password generata appositamente,
che potrai usare soltanto per un particolare client su un particolare
dispositivo.
 Ecco come fare:

1.  Nel tuo pannello utente clicca sul menu delle impostazioni (quello
    accanto al tasto "Webmail", per intenderci).
2.  Clicca su "Gestisci password specifiche".
     ![](/static/img/man_2fa/2FA3.png "password specifiche")
3.  Nel menu "Create new credentials" scegli tra "email" e "jabber",
    aggiungi un commento per identificare la tua nuova password (per es.
    Thunderbird-pc se stai creando una password specifica per
    Thunderbird sul tuo pc) e clicca il tasto "Create".
     ![](/static/img/man_2fa/2FA4.png "crea password specifiche")
4.  Si aprirà una pagina con la nuova password per il tuo client di
    posta o di Jabber.
     ![](/static/img/man_2fa/2FA5.png "nuova password specifica")
5.  Cambia la password nel client e memorizzala subito nel tuo client o
    nel tuo gestore di password preferito (ricorda di cambiarla anche
    per la posta in uscita!).
     Considera che dopo aver chiuso la pagina non avrai modo di
    recuperare la password se non cancellando la precedente e
    generandone una nuova!

Un paio di note importanti
--------------------------

Una volta che avrai attivato l'autenticazione a due fattori, il tuo
punto debole sarà proprio la domanda di recupero della password: se
infatti qualcuno vuole accedere alla tua casella di posta e non ci
riesce perché non ha il tuo telefono, potrebbe semplicemente provare a
resettare la tua password.
 Per questo è essenziale che la tua domanda di recupero riguardi davvero
qualcosa che nessun altro può sapere, e addirittura alcuni consigliano
di usare una domanda e una risposta di fantasia.
 In sintesi l'essenziale è ricordare che:

1.  Se perdi il telefono, l'unico modo di avere accesso alla tua casella
    di posta è rispondere alla domanda di recupero.
2.  Se la risposta alla domanda si trova sui social network, la tua
    casella non sarà sicura anche se hai impostato l'autenticazione a
    due fattori.

Se non hai uno smartphone e non hai nessuna intenzione di
procurartene uno, una soluzione c'è, ma è complicata e prevede
l'acquisto di una *YubiKey*.
 Troverai il comando per configurare la YubiKey nella pagina di
istruzioni che si aprirà quando cliccherai il tasto "Abilita".\
 Se vuoi saperne di più, leggi [questa pagina di spiegazioni sul sito
della
YubiKey](https://www.yubico.com/products/yubikey-hardware/yubikey4/).

