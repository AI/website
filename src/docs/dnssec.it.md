title: DNSSEC 
----

DNSSEC - cos'è e come funziona
==============================

Ogni volta che un dispositivo connesso a internet vuole comunicare con
un altro (ad esempio il vostro computer e i server di A/I) lo deve fare
contattando il dispositivo al suo indirizzo IP. Sarebbe scomodo doversi
ricordare tutti gli indirizzi IP a memoria o doverli digitare, per
questo esiste il sistema DNS per tradurre dei nomi (es.
www.autistici.org) in indirizzi IP (es. 82.221.99.153).

Il sistema DNS non include alcun tipo di garanzia sull'autenticità e
l'integrità della risposta, pertanto chiunque sia in grado di
controllare il vostro traffico è in grado di manipolare quali indirizzi
il vostro dispositivo ritiene validi. Questa possibilità viene ad
esempio sfruttata in alcuni casi per censurare siti in modo molto
semplice: i server DNS dell'Internet provider, invece di restituire il
vero indirizzo IP del sito censurato, risponderanno con un indirizzo IP
non valido oppure con uno che non corrisponde al sito censurato.

Per garantire una maggiore sicurezza, è stato introdotto il protocollo
DNSSEC che si occupa di garantire che le risposte DNS siano autentiche.
Il protocollo non è perfetto (si vedano ad esempio il problema di [*zone
enumeration*](https://en.wikipedia.org/wiki/Domain_Name_System_Security_Extensions#Zone_enumeration_issue.2C_controversy.2C_and_NSEC3)
e le [critiche di D. J. Bernstein](http://cr.yp.to/djbdns/forgery.html))
ma pensiamo che sia importante fornire agli utenti un livello di
protezione aggiuntivo per le comunicazioni con A/I.

Come si usa?
------------

Uno dei modi più semplici per utilizzare DNSSEC è utilizzare un server
DNS pubblico che è in grado di validare le risposte DNSSEC (ad esempio
google public dns). Tuttavia questa soluzione ha il problema che la
comunicazione tra il vostro dispositivo e questo server DNS sicuro può
essere intercettata e le risposte possono essere cambiate a piacimento.

Il modo più sicuro per utilizzare DNSSEC è installare sul proprio
computer (o sulla propria rete interna) un *resolver* DNS in grado di
sfruttare DNSSEC per verificare i risultati. Esistono dei software in
grado di automatizzare l'installazione e la gestione di un server DNSSEC
locale come ad esempio
[dnssec-trigger](https://www.nlnetlabs.nl/projects/dnssec-trigger/) che
funziona su tutti i principali sistemi operativi ed è orientato
all'utilizzo in roaming su un portatile.

Ma sta funzionando?
-------------------

Uno dei modi più semplici per verificare il corretto funzionamento di
DNSSEC è controllare se **non** sta funzionando! In altre parole,
esistono dei siti firmati in modo non valido *di proposito* con DNSSEC,
ad esempio [dnssec-failed.org](http://www.dnssec-failed.org). Se state
usando DNSSEC allora quel sito non dovrebbe funzionare! O per essere più
precisi: il vostro computer non sarà in grado di sapere qual è
l'indirizzo IP di quel sito. Per verificare da terminale (se si sta
utilizzando un server DNS locale come nel caso di dnssec-trigger
menzionato sopra), il comando non dovrà restituire alcun risultato
(status: SERVFAIL):

``` {.code}
$ dig  +dnssec +noauth +noquestion +nocmd +nostats dnssec-failed.org @localhost
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: SERVFAIL, id: 49462
;; flags: qr rd ra; QUERY: 1, ANSWER: 0, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags: do; udp: 1024
```

Mentre per un dominio funzionante verrà anche settata la flag di
risposta valida "ad" dal proprio server locale:

``` {.code}
$ dig  +dnssec +noauth +noquestion +nocmd +nostats autistici.org @localhost
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 63802
;; flags: qr rd ra ad; QUERY: 1, ANSWER: 4, AUTHORITY: 6, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags: do; udp: 1024
;; ANSWER SECTION:
autistici.org.                9382        IN        A        82.221.99.153
autistici.org.                9382        IN        A        178.255.144.35
autistici.org.                9382        IN        A        82.94.249.234
autistici.org.                9382        IN        RRSIG        A 7 2 9600 20140110201142 20131211201142 24242 autistici.org. VMeyEaa3iiCwlDwhch6erY0ScJkdli9P+xzoRP27Ww7/hGea3rwo4hpX 3M8HFwDIkE3aQQLDwvSvlFcB0XzvKdrbOgKXRW4nvaAvhEO5E8eKYmgb oMphm2RoU1+I6r+WWpKpaE/o6bOzDgZ43j1KkjOuEODA/3vbN7G7gIg8 JDw=
```

Come fare validazione delle richieste da riga di comando?
---------------------------------------------------------

Se si utilizza un resolver DNS locale che supporta la validazione DNSSEC
(come nel caso di dnssec-trigger menzionato sopra), tutte le richieste
DNS effettuate a localhost saranno già state validate. Ma nei casi in
cui ciò non fosse possibile, si può comunque validare manualmente una
richiesta (in modo un poco laborioso):

Bisogna anzitutto procurarsi la "chiave" root, ottenibile con:

``` {.code}
dig +nocomments +nostats +nocmd +noquestion -t dnskey . > trusted-root.key
```

e in seguito utilizzarla come chiave *trusted* per validare le risposte:

``` {.code}
dig +sigchase +trusted-key=trusted-root.key www.autistici.org A
```

Altri usi possibili
-------------------

La struttura gerarchica di DNS è riflettuta nel deployment di DNSSEC,
questo significa che ogni entità può pubblicare le proprie firme e
chiavi autonomamente. Questo è in netto contrasto con un'infrastruttura
a chiave pubblica come le CA, infatti con DNSSEC è sufficiente
annunciare quali chiavi pubbliche verranno utilizzate per firmare le
risposte (es, autistici.org pubblica le proprie chiavi verso org.).

Oltre ad indirizzi IP è possibile pubblicare su DNS delle altre
informazioni, ad esempio si possono collegare le informazioni sui
certificati SSL di un dominio. Il protocollo
[DANE](https://en.wikipedia.org/wiki/DNS-based_Authentication_of_Named_Entities)
è la specifica per lo verifica di certificati TLS usando DNS, saltando
completamente l'infrastruttura di CA stessa. 

