title: Two-factor Authentication
----

Protect your account with two-factor authentication
===================================================

Two-factor authentication (or 2FA) is an authentication mechanism that
increases the security level of your account by stopping anyone who may
have sniffed your password online from accessing your user panel and
your webmail.
 The idea is pretty simple: once you activate it, you will need
*something you know* (your password) and *something you own*
(typically your smartphone) to access your user panel and your webmail.
 This means that any ill-intentioned individual will find it more
difficult to access your account: if someone managed to steal your
password somehow, they would also need to steal your phone to access
your account.

How to enable two-factor authentication
---------------------------------------

If you want to enable two-factor authentication, you just need to follow
these simple steps:

1.  Log into your [user panel](https://accounts.autistici.org).
2.  Click on your user name in the top right corner of the screen.
3.  A dropdown menu will open. Click "Enable Two-Factor Auth (OTP)".
     ![](/static/img/man_2fa/2FA1.png "enable 2FA")
4.  Follow the instructions (you will basically need to install an app
    in your phone, set the password recovery question if you haven't
    done so already, and eventually click "Enable").
     ![](/static/img/man_2fa/2FA2.png "how to enable 2FA")
5.  From now on, when you want to log into your user panel, after
    entering your user name and password a second form will ask you for
    an "OTP" code: to access the panel, click on your user name in the
    phone app and enter the code you will see there in the form in your
    browser.
     ![](/static/img/man_2fa/2FA.png "OTP form")

Using mail and jabber clients with 2FA
--------------------------------------

2FA only works for the web. For other uses, you will have to
generate an *app-specific password*. For mail clients like Thunderbird
and for Jabber/XMPP (Pidgin, Jitsi), you will need a specific password
that can only be used for one particular client in one particular
device.
 To generate an app-specific password, follow these steps

1.  Click the settings menu (the gear icon beside the "Webmail" button).
2.  Click "Manage App-Specific Passwords".
     ![](/static/img/man_2fa/2FA3.png "app-specific passwords")
3.  In the "Create new credentials" menu, choose between "email" and
    "jabber", add a comment to identify your new app-specific password
    (e.g. Thunderbird-pc if you're creating a password for Thunderbird
    in your pc) and click "Create".
     ![](/static/img/man_2fa/2FA4.png "create app-specific passwords")
4.  A web page with your new password for your mail or Jabber client
    will open.
     ![](/static/img/man_2fa/2FA5.png "new app-specific password")
5.  Change the password in your client and store it at once in your
    client or in your favourite key manager (remember to set it also for
    outgoing mail!).
     Please, note that after you have closed that web page, you will not be
    able to recover that password if you haven't stored it somewhere:
    you will have to delete it and to generate a new one.

Some further advice -- Please, read, it's important! :)
-------------------------------------------------------

Once you have activated two-factor authentication, your weak spot will
be your password recovery question: if someone wants to access your
mailbox and fails because they would also need your telephone, they
might simply try to reset your password.
 Therefore, it is fundamental that your password recovery question is
linked to something that really no one but you can know. Some experts
even recommend to use an invented question and answer.
 To sum up, remember that:

1.  if you lose your phone, the only way to access your mailbox again is
    by answering the password recovery question;
2.  if the answer to your question can be found in social networks, your
    mailbox isn't safe even if you have activated 2FA.

If you don't have a smartphone and don't want to ever own one, there
is a solution. It is complicated, though, and you will need to buy a
*YubiKey*.
 You will find the command to configure your YubiKey in the instructions
page that will open after you click "Enable".
 If you want to learn more, you can start from [this page in YubiKey's
website](https://www.yubico.com/products/yubikey-hardware/yubikey4/).
