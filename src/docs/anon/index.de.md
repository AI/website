title: Anonymity Howtos
----

Wie bewahre ich meine Anonymität im Internet?
=============================================

Heutzutage wird viel über hochentwickelte Kontrollsysteme gesprochen, die die
endlosen Datenmengen der Menschen im Netz verfolgen, protkollieren und sortieren
und dadurch ganz offensichtlich das Grundrecht auf und die Notwendigkeit für
Privatsphäre verletzen.

Die Kolleg\*innen, die neben dir sitzen, oder dein\*e Chef\*in können leicht
überwachen, was immer du auch im Netz tust. Kommerzielle Webseiten verfolgen für
Marktanalysen deine Aktivitäten, ohne dich um deine Zustimmung zu fragen. Deine
E-Mails können von jedem\*jeder gelesen werden, der\*die Zugriff auf die Server
deines Anbieters hat usw.

Falls du das vermeiden willst, falls du nicht willst, dass deine
Onlineaktivitäten verfolgt und aufgezeichnet werden, kannst du mit einem
Anonymisierungsdienst wie Tor ins Netz gehen und deine E-Mails mit GPG
verschlüsseln.

Um dir dabei zu helfen, deine Privatsphäre zu schützen, haben wir uns dazu
entschieden, auf dieser Seite einige nützliche Anleitungen und Links zu
veröffentlichen.

- [Privacy Mail](/docs/mail/privacymail "Privacy Mail")
- [Anonymes Surfen](/docs/anon/tor)
- [E-Mail-Verschlüsselung](http://www.gnupg.org/documentation/guides.en.html
  "GnuPG guides")
- [Anonyme E-Mails](/docs/anon/remailer "Anonymous Mail")
