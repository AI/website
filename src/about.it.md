title: Chi siamo
----

Chi siamo
=========

A/I è un progetto nato quasi vent'anni fa per offrire supporto in rete (tramite l'offerta di siti web, caselle di posta, mailing list,
chat, instant messaging, anonymous remailer, blog, newsletter e altro ancora) a soggetti provenienti dall'universo dell'organizzazione di
base e dell'autogestione. Le nostre tesi sono semplici: il mondo non dovrebbe ruotare intorno ai soldi, ma saldamente ancorato ad un asse di
relazioni basate su criteri solidaristici, di mutuo appoggio, di uguaglianza e giustizia sociale. Semplice no? Offriamo supporto a quei
singoli individui, gruppi o comunità, le cui attività politiche e sociali rientrino in quest'ottica e che condividano alcuni valori
fondamentali: **l'antifascismo, l'antisessismo, l'antirazzismo, l'antimilitarismo**. A questi si deve poi aggiungere un sentimento,
un'attitudine: **il profondo disagio di fronte alla logica del denaro**.

Le nostre risorse sono garantite dalle donazioni della comunità che le usa, il lavoro del collettivo di gestione e' rigorosamente
**volontario**. Non ospitiamo strutture commerciali, che certo si troveranno più a loro agio altrove. Così pure non sosteniamo strutture
istituzionali o partitiche, che per loro natura dovrebbero poter contare su risorse diverse rispetto a quanto può mettere in campo
un'associazione volontaria e autofinanziata come la nostra. I nostri servizi sono pensati per **tutelare la libertà d'espressione e la
riservatezza** delle persone che li usano, per questo non manteniamo informazioni sensibili su di loro, e non effettuiamo alcun tipo di data
mining con finalita' commerciali.

Per saperne di più su di noi e sulle nostre finalità:

* La nostra [policy](/who/policy)
* La nostra [privacy policy](/who/privacy-policy)
* Un [breve racconto](/who/telltale) su perché siamo chi siamo e perché facciamo quello che facciamo.
* Il [manifesto](/who/manifesto) che abbiamo scritto nel 2002 per spiegare chi siamo e cosa vogliamo
* Una breve [storia del collettivo](/who/collective) (meno lirica e più cronaca)
* [Il libro sui 10 anni di storia del collettivo](/who/book)
* [Propaganda, flyer, banner e grafica](/who/propaganda)
* Il [Piano R\*](/who/rplan/)
* I [costi](/who/costs) dell'infrastruttura

Responsabile legale dei server A/I è l'[Associazione AI-ODV](mailto:associazione@ai-odv.org). 
Per i problemi tecnici scrivete ad [helpdesk](mailto:help@autistici.org)
per qualunque questione di altro tipo, scrivere una mail a: [info](mailto:info@autistici.org)
