﻿title: Privacy Policy
----

Privacy Policy
==============

13 aprile 2020

Questo documento costituisce parte integrante dei Termini di Servizio forniti
da A/I-ODV.  La seguente informativa sulla privacy si applica a tutti i
servizi offerti dall' associazione senza fini di lucro A/I-ODV ("A/I" o
soggetto generico "noi") a te (l'Utente).

Attraverso l'accesso alla nostra piattaforma e l'utilizzo dei nostri Servizi,
l'utente accetta questa Informativa sulla privacy insieme agli altri Termini di
servizio.

I servizi A/I includono - ma non sono limitati a: e-mail, siti Web, blog,
mailing list, newsletter, irc, jabber, remailer anonimo e nym server.

Questo documento illustra le pratiche sulla privacy di A/I che descrivono quali
informazioni raccogliamo, l'uso che facciamo di tali informazioni e il livello
di sicurezza che forniamo.  Riconosciamo e rispettiamo le tue legittime
aspettative di riservatezza in merito a: comunicazione, transito di dati, dati
salvati sui nostri server.  Il nostro metodo di lavoro è strutturato al fine di
raccogliere solo la minima possibile quantità di informazioni personali
necessarie. Non vendiamo, forniamo o noleggiamo a terze parti nessun dato
relativo all'utilizzo dei nostri Servizi.

L'Associazione A/I si trova all'indirizzo Corso Italia n. 115, Pisa 56125, Italia.

Contattateci a [associazione@ai-odv.org](mailto:associazione@ai-odv.org) se
avete domande o dubbi sulla presente Informativa sulla privacy.

Raccolta dati
-------------

La politica fondamentale della nostra organizzazione è quella di raccogliere la
minore quantità possibile di informazioni sugli utenti per garantire
un'esperienza utente completamente privata e sicura nell'utilizzo dei Servizi;
questo perché dette informazioni possono essere utilizzate in maniera lesiva
dei tuoi interessi, per esempio nell'ambito di attività quali frode, violazione
della privacy, furto di identità o altro.  Inoltre non ci è tecnicamente
possibile accedere ai contenuti dei tuoi messaggi crittografati. La nostra
elaborazione dei tuoi dati è limitata al salvataggio temporaneo di detti dati
al fine di permetterti di accedervi.

In pratica conserviamo solo le informazioni minime e necessarie su ciascun
utente, ovvero quelle indispensabili affinché il servizio possa funzionare
correttamente. Di questi dati non vendiamo o condividiamo nulla. Non elaboriamo
la tua posta in arrivo o in uscita in nessuna altra maniera, eccezion fatta per
quella necessaria per proteggerti da virus e spam o eventualmente nel caso in
cui venissimo contattati direttamente da te in caso dovessi incontrare un
problema (processo di risoluzione dei problemi tecnici).

A/I non conserva né raccoglie alcun dato ottenuto tramite l'impronta digitale
del client, ovvero quelle informazioni che il tuo browser Web comunica a tutti
i server Web visitati che consentono ai siti in questione di conoscere i
dettagli del tuo sistema operativo, le informazioni relative al tipo di
browser, plugin installati, caratteri installati, risoluzione dello schermo e
molto altro.

Richiediamo la scelta di un nome utente (indirizzo e-mail) e una password per
identificare e autorizzare il titolare dell'account ad accedere ai servizi
offerti da A/I; non sono richiesti altri dati personali a questo scopo (o altri
scopi). A/I semplicemente seleziona e filtra le persone a cui i Servizi sono
forniti chiedendo di presentare per iscritto una dichiarazione di intenti,
questa richiesta deve essere soddisfatta prima che si possa procedere
all'approvazione di richiesta di qualsiasi account. Le suddette dichiarazioni
sono sempre valutate da un incaricato di A/I in persona, manualmente, e vengono
distrutte subito dopo la fine del processo di approvazione.  Qualsiasi
ulteriore informazione personale offerta spontaneamente dall'utente non viene
archiviata o conservata a meno che non sia strettamente necessario. Ogni
ulteriore comunicazione tra A/I e l'utente avverrà attraverso l'indirizzo
e-mail da noi fornito.

Al fine di garantire i più elevati standard di sicurezza, A/I utilizza solo
server "crittografati alla fonte", cioè un tipo di design dell'architettura
dei sistemi informatici che protegge tutti i dati con l'anonimizzazione
automatica prima di iniziare l'elaborazione (inclusi indirizzi IP e altri dati
personali). Anche nei casi in cui un utente dovesse mettere in opera attività
che costituiscono una violazione dei nostri Termini di Servizio (per esempio
spamming, DDoS o altre), saremmo in grado di recuperare dai nostri server
soltanto un indirizzo IP anonimo, indirizzo eventualmente correlabile
all'account di un utente ma non a un soggetto fisico. Conserviamo i registri
delle attività degli utenti per un periodo massimo di 15 giorni (se non
diversamente specificato per servizio).

I dati statistici relativi all'attività degli utenti attivi sui nostri sistemi
ci aiutano a diagnosticare i problemi software, a mantenere la sicurezza di
detti sistemi contro le intrusioni e a monitorare lo stato della piattaforma.
Utilizziamo la crittografia a livello disco su tutti i dati per mitigare il
rischio di perdite di dati nei casi in cui i server possano essere rubati,
sequestrati o in qualche modo manomessi fisicamente.  Utilizziamo e imponiamo
l'uso della crittografia SSL / TLS su tutte le connessioni ai servizi che
forniamo.

Non richiediamo alcuna informazione personale per poter fornire i nostri
Servizi, poiché l'indirizzo mail di una casella e-mail A/I è l'unico
identificatore di cui un utente ha bisogno, indirizzo che l'utente sceglie
personalmente.  Per motivi di privacy, ti scoraggiamo ad usare il tuo vero nome
(o altro identificativo personale, come un utente su un altro provider Internet
collegato alla tua vera identità) come nome utente / indirizzo email, e
precisiamo peraltro che non abbiamo modo di verificare quando questo sia il
caso o no. Non sono necessarie ulteriori informazioni per il funzionamento dei
Servizi (non chiediamo ulteriori indirizzi email, numeri di telefono, indirizzo
geografico o qualsiasi altro identificativo che potrebbe correlare il tuo
indirizzo email alla tua vera identità).

Sebbene A/I non raccolga intenzionalmente informazioni personali sensibili,
come dati genetici, informazioni sulla salute, preferenze sessuali e altri, ci
rendiamo conto che gli utenti potrebbero archiviare questo tipo di informazioni
nel loro account e-mail, siti Web e altre parti della nostra piattaforma. Se
conservi informazioni personali sensibili sui nostri server, sei responsabile
del rispetto di eventuali controlli normativi relativi a tali dati.

Se visiti la nostra piattaforma o sei utente dei nostri Servizi utilizzando un
telefono cellulare, un tablet o un laptop, valgono le stesse già espresse
premesse rispetto alla raccolta di dati, indipendentemente dal dispositivo,
dalle estensioni installate sulle tue applicazioni, per esempio client o
browser utilizzati.

I dati dell'utente dei servizi sono limitati ai seguenti:

### All'atto di accedere alla piattaforma A/I

Ogni volta che interagisci con la nostra piattaforma o Servizi,
indipendentemente dal fatto che tu abbia un account o meno, lo scambio
automatico di informazioni tra il tuo client e i nostri server ci fornirà
alcuni dati non personali, inclusi, a titolo esemplificativo, i dati relativi
al browser che stai utilizzando (tipo di browser, se si tratti di un
dispositivo mobile / desktop, versione del sistema operativo, lingua
preferita), la data e l'ora della visita e il sito Web di riferimento, ma non
l'indirizzo IP. Nessuno dei dati non personali consente l'identificazione del
singolo utente, in quanto non è associato o collegato alle tue informazioni
personali.

### Registrazione dell'account

Come già detto non è necessario fornire informazioni personali per creare un
account. Tutti i dati forniti all'atto della richiesta vengono eliminati dai
nostri sistemi 15 giorni dopo che la richiesta è stata accolta con successo (o
respinta). Non chiediamo ai nostri utenti di impostare un indirizzo e-mail "di
recupero" e non registriamo la loro password in un formato testuale leggibile
("in chiaro") , pertanto l'amministrazione delle credenziali non è sotto la
nostra responsabilità. A/I sarà in grado di comunicare con gli utenti, se
necessario, solo tramite l'e-mail creata durante il processo di registrazione.

### Richiesta di aiuto

La tua comunicazione con A/I tramite ticket di aiuto per richieste di
supporto, segnalazioni di bug o qualsiasi altro problema verrà salvata dal
nostro staff. Il contenuto di qualsiasi ticket di aiuto creato o commentato
durante l'autenticazione verrà associato al tuo account utente. Eliminiamo
periodicamente i vecchi ticket risolti o chiusi. Ti consigliamo di astenerti
dal comunicarci qualsiasi dato personale poiché l'e-mail in chiaro non è un
mezzo di comunicazione sicuro. Le domande e le risposte sui ticket di aiuto
verranno inviate via e-mail.

### ID session e cookie

Durante l'accesso, manteniamo un identificatore di sessione temporaneo sul tuo
computer che il software client utilizza per dimostrare il tuo stato di
autenticazione. Questo viene automaticamente cancellato dopo la disconnessione
o se la sessione scade. Non utilizziamo cookie di terze parti o meccanismi di
tracciamento di alcun tipo. Gli utenti sono liberi di modificare le proprie
preferenze relative ai cookie in qualsiasi momento nel pannello delle
impostazioni del proprio browser, potendo controllare quali cookie consentire,
quali cookie bloccare in futuro o anche eliminare i cookies. Alcuni link
potrebbero portarti al di fuori della nostra piattaforma digitale e sono al di
fuori del nostro controllo, reindirizzandoti ad altri siti che potrebbero
inviare i propri cookie e raccogliere dati o sollecitare informazioni
personali; pertanto non ci assumiamo alcuna responsabilità per l'utilizzo di
siti Web di terzi.

### Registri di transito e-mail

Al fine di rilevare eventuali abusi (spam) dei nostri servizi email,
manteniamo un log di metadati (limitati a mittente e destinatario) di ogni messaggio
che passa attraverso i nostri server. Questi dati vengono eliminati dopo
15 giorni.

Ricordate che anche quando si utilizza la crittografia
PGP end-to-end per i messaggi e-mail, i campi "subject" e le informazioni
di routing relative al messaggio possono essere visualizzati in chiaro dai
nostri server (e da eventuali osservatori terzi posti lungo la rete)
quando l'email inizialmente lo raggiunge; ciò è dovuto alle
limitazioni intrinseche nel protocollo e-mail e OpenPGP.

### Ultimo accesso

Conserviamo la tua ultima autenticazione riuscita, in modo che sia possibile
per noi disabilitare ed eliminare gli account inutilizzati o abbandonati.

### Attività sui servizi

Teniamo traccia dell'attività degli utenti sui nostri Servizi, ma i log che
conserviamo non contengono mai informazioni di identificazione personale e non
includono informazioni relative ad attività al di fuori della nostra
piattaforma. A/I utilizza questi dati per diagnosticare i problemi del
software, proteggere il sistema dalle intrusioni e monitorare l'integrità dei
servizi. Conserviamo i registri dei servizi per un periodo massimo di 15
giorni, se non diversamente specificato.

### Archiviazione e utilizzo dei dati

I dati sono archiviati solo all'interno dell'UE o all'interno dei paesi
rispettosi del GDPR; abbiamo accesso diretto ed esclusivo a tutti i server
dedicati in cui sono archiviati i dati. Il trattamento dei dati avviene
esclusivamente all'interno del territorio dell'UE e dei paesi rispettosi del
GDPR. La comunicazione tra tutti i server è crittografata con i protocolli più
moderni al fine di proteggere qualsiasi informazione da accessi non
autorizzati, alterazioni dei dati non autorizzate, distruzione o divulgazione
di dati. Non utilizziamo provider di cloud pubblici (come AWS, Google cloud,
Digital Ocean o simili).

Tutti i dati utilizzati dai nostri servizi sono archiviati in un formato
crittografato e solo A/I ha le chiavi per decrittare i dati. Inoltre, i dati
specifici dell'utente (come il contenuto dei messaggi e-mail) sono
crittografati con chiavi disponibili solo all'utente e non agli operatori / il
personale A/I. Forniamo e imponiamo l'uso della crittografia SSL / TLS su tutti
i servizi forniti. Se hai motivo di ritenere che la tua interazione con i
nostri server non sia più sicura, ad esempio se ritieni che la sicurezza dei
servizi o del tuo account siano stati compromessi, ti preghiamo di contattarci
immediatamente.

Non elaboriamo o analizziamo il tuo comportamento in alcun modo, per esempio
allo scopo di creare un profilo contenente le caratteristiche personali degli
utenti o altre pratiche simili. Non pubblichiamo annunci pubblicitari o
intratteniamo rapporti commerciali con inserzionisti. A/I non condivide,
affitta o vende dati a terzi. Non inviamo informazioni di marketing ai nostri
utenti, anche considerando che non vendiamo i nostri Servizi.

Non condividiamo i tuoi dati con terzi a meno che i servizi interoperabili di
rete (federati) non richiedano determinati dati per il loro corretto
funzionamento (ad es. Altri fornitori di servizi devono conoscere il tuo
indirizzo e-mail per poter fornire un servizio). In questi casi, operi
attivamente la scelta e l'atto di condividere i tuoi dati e non abbiamo modo di
impedirti di farlo. Al fine di preservare la tua privacy, ti scoraggiamo dal
farlo.

Non accediamo ai tuoi dati, e-mail, file, ecc. archiviati sui nostri server, a
meno che ciò sia necessario per la risoluzione di problemi tecnici o nel caso
ci sia il sospetto di violazione della nostra policy.

In caso di risoluzione di problemi tecnici, chiediamo la tua autorizzazione in
modo previo all'atto di accedere ai tuoi dati e ti informiamo di tutte le
azioni intraprese sull'account nel rapporto sulla trasparenza indirizzato al
titolare dell'account a seguito del processo di risoluzione del problema in
questione.

In caso iniziassimo a sospettare di un comportamento non conforme alle nostre
policy, possiamo chiedere all'utente di correggere il proprio comportamento o
anche decidere di cancellare un account in modo permanente e senza preavviso:
di nuovo, gli utenti sono ammessi in modo condizionale ad un comportamento
conforme alla nostra policy e in base a quella che riteniamo sia la loro
affinità con il nostro Manifesto.

Informazioni aggregate anonime che non possono essere ricollegate a un singolo
utente possono essere rese disponibili a ricercatori al solo scopo di
sviluppare sistemi migliori per comunicazioni anonime e sicure. Ad esempio,
possiamo aggregare informazioni su quanti messaggi in media un gruppo di utenti
anonimi invia e riceve e con quale frequenza.

Come indicato nei nostri [ToS](/who/policy.it), A/I fornisce uno strumento di
statistica web come parte dei nostri Servizi, pertanto non sono consentiti
altri strumenti di analisi simili. I nostri strumenti di analisi non
raccoglieranno mai dati personali. Non utilizziamo direttamente questi
strumenti e analisi, ma questo servizio è disponibile per tutti gli utenti che
scelgano di usarli.

Al fine di garantire la sicurezza di tutti i dati, A/I impiega varie misure di
sicurezza amministrative, tecniche e fisiche, tuttavia è responsabilità
dell'utente prestare attenzione e agire ragionevolmente quando si utilizzano i
servizi di A/I. Sarai personalmente responsabile se qualsiasi azione dovesse
violare per esempio il diritto alla privacy di terzi o altri diritti. Non
saremo responsabili delle conseguenze delle vostre attività illegali, delle
vostre azioni frutto di scelte deliberate o di negligenze, nonché di eventuali
circostanze che potrebbero non essere state ragionevolmente controllate o
previste (dall'utente o da noi).

Ai sensi del Regolamento (UE) 2016/679, noto anche come GDPR, la base giuridica
per il nostro trattamento dei dati è l'articolo 6, paragrafo 1, lettera b) che
consente ad A/I di elaborare i dati per adempiere a un contratto o per misure
preliminari a un contratto. Ciò significa che trattiamo i dati degli utenti al
fine di far rispettare i nostri Termini di utilizzo o per proteggere la
sicurezza e l'integrità dei Servizi forniti.

### Accesso alle tue informazioni

L'accesso ai tuoi dati personali e ai file archiviati e ad altre informazioni
che fornisci a uno qualsiasi dei Servizi offerti da A/I è sotto il tuo
esclusivo controllo.

Non accediamo ai tuoi dati, e-mail, file, ecc. archiviati sui nostri server se
non allo scopo di risolvere problemi tecnici o nel caso esista un sospetto di
violazione della nostra policy. In caso di risoluzione di problemi tecnici,
chiediamo la tua autorizzazione in precedenza dell'atto di accedere ai tuoi
dati e ti informiamo in seguito di tutte le azioni intraprese sull'account nel
rapporto sulla trasparenza indirizzato al titolare dell'account.

Alcuni dei servizi forniti da A/I come e-mail e Jabber funzionano in base ai
cosiddetti protocolli federati. Ciò consente agli utenti registrati a diversi
fornitori di servizi di interagire tra loro. A causa della natura di questi
protocolli (capacità di scambiarsi messaggi, condividere file, chattare) alcuni
dei dati vengono naturalmente condivisi con altre entità. Tuttavia, la
condivisione dei dati con altri fornitori di servizi è una scelta dell'utente
ed è configurata dagli utenti nelle loro impostazioni del servizio, inclusa la
decisione riguardo a cosa condividere e con chi. È possibile che vengano
mostrati video embedded e collegamenti a anteprime da altri siti Web durante
l'utilizzo dei servizi forniti da A/I. Ciò potrebbe esporti al monitoraggio web
da parte di servizi esterni, come (ma non limitato a) Facebook, Twitter e
Google. Ancora una volta, al fine di preservare completamente la tua privacy,
dovresti tenere separato il tuo account A/I da altri account. Se questo non è
ciò che intendi fare, il tuo indirizzo e-mail A/I potrebbe diventare un
obiettivo di raccolta e profilazione dei dati su altri sistemi che non sono
sotto il nostro controllo e responsabilità. Tutti i dati e i file memorizzati
su servizi che sono legati a informazioni personali (servizi che richiedono
l'accesso) sono disponibili per il download a scopo di archiviazione o per il
trasferimento su un altro sito Web compatibile.

Si noti che nessun metodo di trasmissione su Internet o metodo di archiviazione
elettronica è sicuro al 100%, pertanto A/I non può garantire la sua sicurezza
in maniera assoluta. Se hai domande sulla sicurezza della nostra piattaforma,
puoi contattarci all'indirizzo associazione@ai-odv.org per ulteriori
informazioni. Nel caso in cui le informazioni personali vengano compromesse
come nel caso di una violazione della sicurezza, A/I informerà tempestivamente
l'utente e rispetterà la legge applicabile.

### Cancellazione dell'account

Puoi scegliere di eliminare il tuo account A/I in qualsiasi momento. Puoi
disabilitare il tuo account e-mail dal tuo pannello utente. Questo non cancella
completamente l'indirizzo di posta dal nostro sistema (per evitare che qualcun
altro possa chiedere lo stesso indirizzo in futuro). Contattaci se preferisci
una cancellazione completa. Quando l'account e-mail è disabilitato, il
contenuto della casella di posta verrà automaticamente cancellato entro 3
giorni. I siti Web, le mailing list e i blog gestiti da quell'account di posta
rimarranno attivi, a meno che tu non li disattivi personalmente o richieda la
disattivazione a noi, successivamente al tuo atto di disattivazione del tuo
account di posta elettronica corrispondente. Se devi rimuovere alcune
informazioni personali da un archivio pubblico di una mailing list o da un sito
Web / blog ospitato sulla nostra piattaforma, ti preghiamo di contattarci. Per
richiedere la cancellazione di qualsiasi dato, ti chiediamo di scriverci
dall'account e-mail collegato a tali dati (non abbiamo altro modo per
verificare che tu sia il vero proprietario dei dati).

### I tuoi diritti

Ai sensi del Regolamento generale sulla protezione dei dati (GDPR) hai
determinati diritti in relazione ai tuoi dati personali, hai il diritto di
richiedere ad A/I di informarti sui dati personali che abbiamo raccolto su di
te, di richiedere qualsiasi modifica e correzione o cancellazione di
informazioni inesatte, il diritto di limitare o opporti a determinati
trattamenti delle tue informazioni, nonché il diritto di richiederci di
fornirti una copia dei tuoi dati personali in un formato strutturato,
comunemente usato e leggibile da una macchina, e il diritto a trasmettere (se
tecnicamente fattibile) i tuoi dati personali a un altro controller. Se hai
fornito il consenso per il trattamento dei tuoi dati hai il diritto (in
determinate circostanze) di revocare quel consenso in qualsiasi momento, il che
non influirà sulla liceità del trattamento prima che il tuo consenso fosse
revocato. Tuttavia, come spiegato in precedenza, nessuno dei nostri servizi
richiede o necessita di dati personali. Pertanto, non abbiamo mai chiesto il
tuo consenso al trattamento dei tuoi dati personali che sono di fatto
memorizzati volontariamente da te nel tuo spazio utente personale e non
accessibili o elaborati da noi in alcun modo diverso dalla pura memorizzazione.
Puoi in qualsiasi momento disporre di tutti i dati che ci hai fornito
direttamente, incluso il download e la cancellazione permanente.

Se non sei d'accordo con il tuo trattamento dei dati da parte di A/I, sei
libero di non utilizzare i Servizi di A/I e di interromperne l'utilizzo in
qualsiasi momento. È possibile che ci venga richiesto di interrompere
l'elaborazione delle informazioni dell'utente, nel qual caso i dati verranno
elaborati solo fintanto che sarà necessario per interrompere il tuo uso dei
Servizi o finalizzare altre posizioni legali.

In ogni caso, A/I non memorizzerà alcun dato o lo conserverà per più di 2 anni
dall'ultimo utilizzo dei Servizi.

Oltre alla crittografia automatica, nessun utente è soggetto a decisioni basate
esclusivamente sull'elaborazione automatizzata, inclusa la profilazione, che
può produrre effetti legali che lo riguardano o che influiscono in modo
significativo su di lui.

A meno che si tratti di altre procedure amministrative o giudiziarie, ogni
utente ha il diritto di presentare una richiesta di conformità a un'autorità di
controllo, in particolare nello Stato europeo della sua residenza abituale,
luogo di lavoro o luogo di presunta violazione se l'utente crede che A/I non
abbia rispettato i requisiti del GDPR in relazione ai propri dati personali.

AI-ODV è il responsabile del trattamento dei dati ai fini della GDPR. Se hai
dubbi su come vengono trattati i tuoi dati, puoi contattarci all'indirizzo
[associazione@ai-odv.org](mailto:associazione@ai-odv.org), che è la nostra
e-mail di contatto ufficiale.

### Modifiche a questa policy

A/I si riserva il diritto di modificare questa policy. Ti consigliamo di
rivedere regolarmente le nostre policy per verificare se siano state oggetto di
un aggiornamento. Tutte le modifiche sono in vigore poiché vengono aggiunte
all'ultima versione della presente Informativa sulla privacy e entreranno in
vigore dal momento in cui vengono pubblicate. Se apportiamo importanti
modifiche, avviseremo i nostri utenti in modo chiaro e diretto (ad esempio
tramite le nostre newsletter). Piccole modifiche possono essere evidenziate
solo nel piè di pagina del nostro sito Web.

(Versione originale: [EN](/who/privacy-policy.en) / Tradotto in: IT)

Se hai ulteriori dubbi dai un'occhiata alle nostre [FAQ](/docs/faq) o [contattaci](mailto:info@autistici.org)!
