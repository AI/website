title: The R* Plan - Introduction
----

The R\* Plan: a network of resistant communication
===========================================================================

The Other Side of the Spiral
----------------------------

New forms of expression and organization are arising, disorderly, creating relationships, ways of production and means of imagination. Day
after day, without any prearranged plan, but with many shared ideas, we see new ways of living in this chameleon-like world -- ways of
living that corporations and billionaire foundations try to exploit to their and their shareholders' advantage.

Awkward attempts to fill the "dangerous regulative gap" with laws inspired by the clash of civilization, by international terrorism, by the
need to know everything about everyone, by humidity and by locusts, have created a context in which rules and their enforcement are but
"another point of view", adequate for any situation, for any party in the game and for the most popular tabloids.

Incapable of telling good from wrong, institutions are trying to close the fence, proceeding in a confused, contradictory and hasty way.
They are working hard to give rise to a closed-minded, terroristic culture, and construct the dangers needed to prevent an "improper" use of
the Web, in an attempt to create the Global Mall, a secure and well-lit place.

We go along a path of repression, a not so slow repression against anybody challenging this not so new mentality, and its cunning
sponsorship.

All this, and much more, has lead in the last few years, not only in the socalled rogue states, but also in the self-professed democratic
countries, to a more or less evident violation of what is usually called civil rights and freedom of speech: the regular tapping of any
communication channel, an increased trend towards websites and mailboxes seizures, more or less successful attempts to censor difficult and
independent information, and the criminalization of exchange and sharing.

From its commercial, wonderland-like image, Internet turns, according to the current needs, into a hell of pedophiles, cheaters and
terrorists.

Unwillingly thrown into this scenery, and basing on our experience, we have tried to make out a different way to arrange our services, to
re-assert out will to R\*esist.

So we have conceived the R\* Plan \[[#1](#1)\]: if you wish to know more about it, you just need to surf this
section of the site.

<a name="1"></a>**\[1\]** To develop our plan, we read through many history books, so as to remember the history of winners and find the best strategy for a
great leap forward. After reading the classics, from Adam Smith to Kipling, we bumped into an Italian text, a plan written as late as 1982
by a former masonic Grand Master and aiming at subordinating Italy to corporations, and to turn citizens into numb disempowered manpower.
 That attempt was called R Plan, and since then everybody has been telling us it was thwarted. But all evidences around us show it was not,
and since it was so successful, even after 30 years, we have decided to get inspiration from it, but giving it a twist, calling it R\* Plan.
