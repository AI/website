title: Benvingud@s a Autistici/Inventati
----

Benvingud@s a Autistici/Inventati
=================================

A/I va nàixer fa més de 10 anys quan individus i col·lectius ocupats/des en qüestions de tecnologia, privacitat, drets digitals i activisme
polític es van reunir a Itàlia. La nostra idea bàsica és proveir una ampla varietat de ferramentes de comunicació lliures, impulsant a les
persones a triar aquelles opcions lliures en lloc de les comercials. Volem informar i formar al voltant de la necessitat de protegir la
nostra pròpia privacitat amb la fi d'evitar el saqueig que governs i corporacions porten a terme de manera indiscriminada amb les nostres
dades i personalitats.

A/I [provide](/services/) gratuïts, lliures i respectuoses amb la privacitat, entre els quals s'inclouen:

[Blocs](/services/blog "Blog on Noblogs.org") / [Allotjament web](/services/website "Website hosting on A/I")
[Serveis d'anonimització](/services/anon "Anonymity services on A/I") / [VPNs personals](https://vpn.autistici.org "Personal A/I VPNs")
[Correu electrònic](/services/mail "Email on A/I") / [Llistes de correu, newsletters i fòrums](/services/lists "Mailing lists on A/I")
[Missatgeria instantània i Chats](/services/chat "Instant Messaging and Chat on A/I") i [més](/services).

Totes les sol·licituds de serveis s'aproven sempre i quan respecten la nostra [política](/who/policy) i compartisquen el
nostre [manifest](/who/manifesto).

**[Solicita un servei](get_service)**

**Follow our [blog](http://cavallette.noblogs.org).**


