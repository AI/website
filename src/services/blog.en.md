title: Noblogs: blogs without logs
----

Noblogs: blogs without logs
================

[NoBlogs project](http://noblogs.org) stems from the desire of keeping [A/I service offer](/services/) up to date with
what the tools of digital communication have become in the last years. We have become aware that most people did not actually need a
website, but mostly **a place where they can communicate in a simple and direct way**.

You can blog whatever content you prefer, **provided you comply to the** [general policy](/who/policy) of our servers
and the [specific policy](http://noblogs.org/policy/ "NoBlogs Policy") of our blogging platform.
 Actually we hope this service will not be used to write your diary (you can also do that, of course, if you want so), but to generate
interesting content up to the production of a **common independent information, communication, relationship and political initiative
space**.

**Freedom of speech implies a keen eye on privacy and security**. We do our best, using free software only, not keeping any log or any of
your personal data, and suggesting useful links speaking of blogs and privacy... But this is not enough if **you do not do your best**,
reading the [guides we suggest you](/docs/blog/) and taking any possible precautionary measure to ensure your
security. We are glad to remind some of these to you all.

- Stop using gmail, hotmail or any other commercial email provider to exchange important and delicate information. **Mailboxes of these
    providers will not be accepted to [register a new blog](http://noblogs.org/register)**
    Use our mailbox offer or any other non-commercial autonomous server offer ([check it out!](/links)).
- Remember that many server around the internet keep a copy of any non-encrypted electronic communication
- Do not use closed source software, since you can not know how they work and if they have any internal function endangering your privacy.
- Keep maximum attention when you send around digitally names, surnames, telephone numbers, addresses and any other personal data
- Web analitics software endanger the privacy of people surfing your blog, that's why we use our own
  [web statistics software](/services/piwik "Matomo web analitics for A/I"): yes it's less "informative" than google analitics, but it
  keeps our visitors safe of prying eyes

**Now you can go to [Noblogs.org](https://noblogs.org/register/) and open your blog**

**Very important: about server security** If what you need is just publishing content, we warmly recommend you to opt for a blog on Noblogs
rather than for a website with a software like Wordpress installed. This is because software **must** be regularly updated, whereas with
Noblogs there's no need to do it :)
