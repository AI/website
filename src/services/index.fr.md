title: Services
----

Services
========

A/I fournit des outils pour les communications et le partage anonymes. Nous vous recommandons d'utiliser nos services avec des
[outils spécifiques de protection de la vie privée](/docs/mail/privacymail)

Tous nos services sont caractérisés par une attention particulière de la vie privée de nos utilisateurs. C'est pourquoi **nous ne gardons
pas de logs** de connexion et nous n'enregistrons pas d'informations permettant de lier les services, l'identité et les noms des
utilisateurs. En outre, nous recommandons fortement l'utilisation des outils de cryptage que nous offrons à tout le monde (souvenez-vous en
lorsque vous configurerez votre client mail ou que vous mettrez à jour votre site web!).

Pour pouvoir utiliser les services offerts par notre réseau vous devez partager les principes basiques de **l'anti-fascisme, de
l'anti-racisme, de l'anti-sexisme, de l'anti-militarisme et de l'approche non commerciale** sur lesquels ce projet a été fondé, ainsi qu'une
envie de favoriser les rapports humains et le partage plutôt que les faux-semblants et le rapport commercial. Mais nous espérons que vous le
saviez déjà si vous êtes arrivés jusqu'ici ;)))) !

Maintenant que vous avez lu tout ceci, [Inscrivez-vous et demandez un service](/get_service)

## Quels services fourni A/I?

Nous fournissons une large palette d'outils de communication:

- [Email](/services/mail "E-mail")
- [Blogs](/services/blog "Blog su Noblogs.org")
- [Mailing lists et Newsletters](/services/lists "Mailing list")
- [Hébergement Web](/services/website "Siti web")
- [Messagerie Instantanée et Chat](/services/chat "Instant Messaging e chat")
- [Services d'anonymat](/services/anon "Anonimato") et [VPNs personnels](https://vpn.autistici.org "A/I VPN")

## Coûts

Les services que nous offrons n'ont pas de prix définis, mais comme vous pouvez aisément l'imaginer, maintenir l'ensemble du système
fonctionnel (ex: le matériel et l'hébergement) est [très coûteux](/who/costs) (en ce moment ça tourne autour de 13.000
euros par an).

Si vous souhaitez que nos services restent en ligne et que notre projet survive, il est crucial que chaque personne/groupe/projet qui
utilise les services offerts par ce réseau nous fasse une donation annuelle, selon ses ressources.

Le montant de votre donation peut varier de quelques euros pour une bo&icric;te mail à 20-25 euros pour un site web ordinaire, jusqu'à des
donations plus importantes si vous êtes un groupe ou un collectif utilisant des boîtes mails et des mailing lists (vous pouvez organiser des
collectes de fond festives par exemple) ou si vous avez la possibilité et l'envie de contribuer plus.

Dans tous les cas, nous ne nous mêlerons jamais de vos histoires d'argent, mais nous vous demandons un peu de compréhension (parce que si
nous ne recevons pas d'argent, nous serons obligés de fermer les installations) et de bonne volonté.

S'il vous plait, réflechissez-y et [Donnez](/donate).
